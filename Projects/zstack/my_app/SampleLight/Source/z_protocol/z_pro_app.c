#include "z_protocol.h"
#include "NLMEDE.h"
#include "hal_types.h"
#include "string.h"
#include "hal_uart.h"
#include "ioCC2530.h"
#include "OSAL.h"
#include "z_pro_app.h"

uint8_t switch_product_id[] = {0x35,0x62,0x34,0x63,0x66,0x65,0x61,0x63,0x66,0x35,0x62,0x61,0x34,0x62,0x32,0x33,0x62,0x61,0x35,0x37,0x66,0x32,0x31,0x38,0x66,0x64,0x39,0x66,0x62,0x34,0x63,0x35,0x00};


byte macaddr_16[20]={0};

//void z_peotocol_init(z_protocol_info_t*broker,uint8 *end_product_id);
void HexToStr(byte*dest , byte*src , uint8 len );

void z_peotocol_init(z_protocol_info_t*broker,uint8 *end_product_id)
{
  osal_memset(broker->product_id,0,sizeof(broker->product_id));
  osal_memset(broker->IEEE_id,0,sizeof(broker->IEEE_id));
  
  if( end_product_id )
  {
    strncpy(broker->product_id , end_product_id , strlen(end_product_id));
  }
  
  byte macaddr[17] = {0};                  //获取mac
  byte *macaddr_p = NLME_GetExtAddr();
  
  osal_revmemcpy(macaddr,macaddr_p,8);
  HexToStr(macaddr_16 , macaddr , 8 );
  
  if( macaddr_16 )
  {
    osal_memcpy(broker->IEEE_id ,macaddr_16 ,16);
  }
}

void HexToStr(byte*dest , byte*src , uint8 len )
{
  uint8 ddl,ddh;
  uint8 i;
  
  for(i=0;i<len;i++)
  {
    ddh = 48 + ( src[i]/16 );
    ddl = 48 + ( src[i]%16 );
    if(ddh > 57)
      ddh = ddh +7;
    if(ddl > 57)
      ddl = ddl +7;
    dest[i*2] = ddh;
    dest[i*2+1] = ddl;
  }
  dest[len*2] = '\0';
}