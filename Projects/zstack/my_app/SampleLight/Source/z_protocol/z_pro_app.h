#ifndef __Z_PRO_APP__
#define __Z_PRO_APP__

#include "z_protocol.h"
#include "hal_types.h"


extern byte macaddr_16[20];
extern uint8_t switch_product_id[];
extern void z_peotocol_init(z_protocol_info_t*broker,uint8 *end_product_id);
extern void HexToStr(byte*dest , byte*src , uint8 len );

#endif