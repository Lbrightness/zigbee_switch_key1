#include "z_protocol.h"
#include "hal_types.h"
#include "string.h"
#include "OSAL_Memory.h"
#include "OSAL.h"
#include "AF.h"
#include "ioCC2530.h"
#include "hal_uart.h"
#include "z_pro_app.h"
#include "switch.h"
#include "OSAL_Nv.h"
#define ZSoftware_Protocol 0x03
#define ZHeartTicket_Protocol 0x03
#define ZProtocol_Version  0x01

z_protocol_info_t z_protocol_info;    

uint8 temp_password[11]={0};
uint8 bind_flag = 0;
uint8 bind_MS_flag = 0;

uint8 scene_falg = 0;

uint16_t cl_shortaddr;
uint16_t door_shortaddr;

uint8_t Z_Verify_Data(uint8_t *pbuf, int len);

uint8_t Z_Verify_Data(uint8_t *pbuf, int len) //校验函数
{

    uint8_t cheaksum = 0;
    int i;
    ZDEV_PACKAGE* up_pack = (ZDEV_PACKAGE*)pbuf;

    uint16_t length = up_pack->Len_h*256 + up_pack->Len_l;

    for(i = 0;i< length-1;i++)
    {
        cheaksum ^= pbuf[i+2];
    }
    return cheaksum;
}

void end_Attributes_to_cood_Func(z_protocol_info_t *protocol_info , uint8 cmd)
{
    uint8_t product_id_len = strlen(protocol_info->product_id);
    uint8_t mac_id_len = strlen(z_protocol_info.IEEE_id);
    uint16_t length = 2 + 2 + 1 + product_id_len + 1 + Z_MAC_ADDR_LEN  + 1 + 2 + 1 + 1; //length + 03+01+ p_id_len + p_id +MAC_len+ MAC +  shortadd——len+shortadd+cmd  + xor
    uint16_t packet_len = length + 4;
    
    
    uint8_t packet[70]={0};
    uint16_t offset = 0;

    ZDEV_PACKAGE *pack = (ZDEV_PACKAGE *)packet;
   
    packet[0]  =  UPKG_H80;
    pack->Head[1]  = UPKG_H7F;
    pack->Len_h = (uint8_t)(length>>8);
    pack->Len_l = (uint8_t)(length&0x00ff);
    if(cmd == end_hearstick_cmd)
    {
        pack->Protocol_Type  = ZHeartTicket_Protocol;
        pack->Protocol_Version   = ZProtocol_Version;
        offset += 6;
        packet[offset++] = product_id_len;
        memcpy(packet+offset,z_protocol_info.product_id,product_id_len);
        offset += product_id_len;

        packet[offset++] = mac_id_len;
        memcpy(packet+offset,z_protocol_info.IEEE_id,mac_id_len);
        offset += mac_id_len;
        
        uint16 my_shortaddr = NLME_GetShortAddr();
        packet[offset++] = 0x02;//短地址长度
        packet[offset++] = (uint8_t)(my_shortaddr>>8);
        packet[offset++] = (uint8_t)(my_shortaddr&0x00ff);
        
        packet[offset++] = cmd;
        
    }
    else if(cmd == connect_success_up_Attributes_Cmd)
    {
        pack->Protocol_Type  = ZSoftware_Protocol;
        pack->Protocol_Version   = ZProtocol_Version;
        offset += 6;
        packet[offset++] = 0;
        packet[offset++] = 0;
        packet[offset++] = 0;
        packet[offset++] = cmd;
        packet[offset++] = 1;
        packet[offset++] = 1;
        packet[offset++] = product_id_len;
        memcpy(packet+offset,z_protocol_info.product_id,product_id_len);
        offset += product_id_len;
        packet[offset++] = mac_id_len;
        memcpy(packet+offset,z_protocol_info.IEEE_id,mac_id_len);
        offset += mac_id_len;  
        packet[offset++] = 0x02;//shortaddr
        uint16 my_shortaddr = NLME_GetShortAddr();
        packet[offset++] = (uint8_t)(my_shortaddr>>8);
        packet[offset++] = (uint8_t)(my_shortaddr&0x00ff);
        packet_len += 5;
        length += 5;
        pack->Len_h = (uint8_t)(length>>8);
        pack->Len_l = (uint8_t)(length&0x00ff);
    }  
    
    packet[offset++] = Z_Verify_Data(packet,packet_len);//校验
    packet[offset++] = UPKG_T23 ;
    packet[offset] = UPKG_TDC;
    
    Send_nwk_date(packet,packet_len);
}

void end_status_to_coor(uint8 WhataData , uint8 *data ,uint8 data_len , uint8 cmd)
{
    uint8_t product_id_len = strlen(z_protocol_info.product_id);
    uint8_t mac_id_len = strlen(z_protocol_info.IEEE_id);
    uint16_t length =2 + 1 + 1 + product_id_len +  mac_id_len + 2 +  1 + 2 + 1 + 1 + data_len + 1; 
    uint16_t packet_len = length + 4;

    uint8_t packet[80]={0};
    uint16_t offset = 0;
    
    ZDEV_PACKAGE *pack = (ZDEV_PACKAGE *)packet;
    
    packet[0]  =  UPKG_H80;
    pack->Head[1]  = UPKG_H7F;
    pack->Len_h = (uint8_t)(length>>8);
    pack->Len_l = (uint8_t)(length&0x00ff);
    pack->Protocol_Type  = ZSoftware_Protocol;
    pack->Protocol_Version   = ZProtocol_Version;
    offset += 6;
    
    packet[offset++] = product_id_len;
    memcpy(packet+offset,z_protocol_info.product_id,product_id_len);
    offset += product_id_len;

    packet[offset++] = mac_id_len;
    memcpy(packet+offset,z_protocol_info.IEEE_id,mac_id_len);
    offset += mac_id_len;
    
    uint16 my_shortaddr = NLME_GetShortAddr();
    packet[offset++] = 0x02;
    packet[offset++] = (uint8_t)(my_shortaddr>>8);
    packet[offset++] = (uint8_t)(my_shortaddr&0x00ff);
    
    packet[offset++] = cmd;
    
    packet[offset++] = WhataData;
    
    memcpy(packet+offset,data,data_len);
    offset += data_len;
    
    packet[offset++] = Z_Verify_Data(packet,packet_len);//校验
    packet[offset++] = UPKG_T23;
    packet[offset] = UPKG_TDC;
    
    
    if((cmd == 0xC1)||(cmd == 0x41)||(cmd == 0x43)||(cmd == 0x44))
    {
         Send_nwk_date(packet,packet_len);  
    }
    else if(cmd == 0xC2)
    { 
        Send_bind_data(packet,packet_len);
    }
}

void end_bind_Func( uint8 led_bit , uint8 cmd)
{
    uint16_t length = 2 + 2 +1+ 1 +1+ SHORTADDR_LEN + 1 +1+ 1; //length + 03+01+proid_len+MAC_len+ shortadd——len+shortadd+ cmd  +led_bit+ xor
    uint16_t packet_len = length + 4;
    
    uint8_t packet[30]={0};
    uint16_t offset = 0;
    
    ZDEV_PACKAGE *pack = (ZDEV_PACKAGE *)packet;
    
    packet[0]  =  UPKG_H80;
    pack->Head[1]  = UPKG_H7F;
    pack->Len_h = (uint8_t)(length>>8);
    pack->Len_l = (uint8_t)(length&0x00ff);
    pack->Protocol_Type  = ZSoftware_Protocol;
    pack->Protocol_Version   = ZProtocol_Version;
    offset += 6;
    
     packet[offset++] = 0x00;//没有product——id
     packet[offset++] = 0x00;//没有mac——id
     
    uint16 short_addr = NLME_GetShortAddr();
    packet[offset++] = SHORTADDR_LEN;
    packet[offset++] = (uint8_t)(short_addr>>8);
    packet[offset++] = (uint8_t)(short_addr&0x00ff);
    
    packet[offset++] = cmd;
    
    packet[offset++] = led_bit;
    
    packet[offset++] = Z_Verify_Data(packet,packet_len);//校验
    packet[offset++] = UPKG_T23;
    packet[offset] = UPKG_TDC;
    
    afAddrType_t DstAddr_bind;
    DstAddr_bind.addrMode = (afAddrMode_t)AddrBroadcast;
    DstAddr_bind.endPoint = SWITCH_ENDPOINT;
    DstAddr_bind.addr.shortAddr = 0xFFFF;
    
    AF_DataRequest(&DstAddr_bind,&end_epDesc,SmartHome_CLUSTERID,packet_len,packet,&SmartHome_TransID,AF_DISCV_ROUTE,AF_DEFAULT_RADIUS);
}

void end_analysis_coor_Func(uint8 *buffer ,uint8 linkqi )
{
    uint16_t product_id_len = buffer[6];
    uint16_t device_id_len = buffer[6+product_id_len+1];
    uint8 packet_cmd = buffer[6+product_id_len+device_id_len+2+3];
    uint8* data = buffer+(6+product_id_len+device_id_len+2+3+1);

    if(packet_cmd == 0xD1) //判断是否app控制子设备
    {     
        control_bind_switch(data[0],data[1]);
    }
    else if(packet_cmd == 0xC2)
    {
        control_switch(data[0],data[1]);
    }
    else if(packet_cmd == 0xB1)
    {
        switch_stuts_send();
    }
    else if(packet_cmd == 0x3A)
    {
        int temp_time = (osal_rand()*100)%5000;
        osal_start_timerEx(SmartHome_TaskID,SmartHome_Stuts_SEND,temp_time);
    }
    else if(packet_cmd == 0xB2)
    {
        end_status_to_coor(0x01,&linkqi,1,0xB3);
    }
    else if(packet_cmd == end_bind_Cmd )
    {
            if(bind_flag & 0x10)
            {             
                osal_stop_timerEx(SmartHome_TaskID,Stop_Bind);
                end_bind_Func((bind_flag >> 4), end_bind_ack_Cmd);//发送绑定回应
                bind_flag |= (bind_flag >> 4); //记录当前绑定按键
                osal_nv_item_init(Double_Control_List,22,NULL);
                
                bind_MS_flag |= (bind_flag >> 4);
                osal_nv_write(Double_Control_List,4,1,&bind_MS_flag);//记录主从          
                //判断当前键位          
                if(bind_flag&0x10)
                {
                     key2_bind_flag =  data[0];
                     osal_nv_write(Double_Control_List,1,1,&key2_bind_flag);
                }
                bind_flag &= ~(0xF0) ;//清空当前绑定键位
                osal_nv_write(Double_Control_List,0,1,&bind_flag);//把当前绑定按键存入flash
                osal_nv_write(Double_Control_List,2,2,buffer+6+product_id_len+1+device_id_len+1+1);//将目标短地址存入flash
                DstAddr.addr.shortAddr = (buffer[6+product_id_len+1+device_id_len+1+1]*256) + (buffer[6+product_id_len+1+device_id_len+1+1+1]);//记录目标短地址     
                  uint8 bing_ok_time = 5;
                  while(bing_ok_time--)
                  {
                      Delay(3);
                      P1_7 = ~P1_7; 
                      Delay(3);
                      P1_7=  ~P1_7;
                  }
            }
    }
    else if(packet_cmd == end_bind_ack_Cmd )
    {  
             if((bind_flag & 0x10))
            {
                osal_stop_timerEx(SmartHome_TaskID,Stop_Bind);
                bind_flag |= (bind_flag >> 4);
                osal_nv_item_init(Double_Control_List,22,NULL);
                
                bind_MS_flag &= ~(bind_flag >> 4);
                osal_nv_write(Double_Control_List,4,1,&bind_MS_flag);//记录主从
                

                if(bind_flag&0x10)
                {
                     key2_bind_flag =  data[0];
                     osal_nv_write(Double_Control_List,1,1,&key2_bind_flag);
                }
                
                bind_flag &=  ~(0xF0) ;//清空当前绑定键位
                osal_nv_write(Double_Control_List,0,1,&bind_flag);
                osal_nv_write(Double_Control_List,2,2,buffer+6+product_id_len+1+device_id_len+1+1);//将目标短地址存入flash
                DstAddr.addr.shortAddr = (buffer[6+product_id_len+1+device_id_len+1+1]*256) + (buffer[6+product_id_len+1+device_id_len+1+1+1]);
                  uint8 bing_ok_time = 5;
                  while(bing_ok_time--)
                  {
                      Delay(3);
                      P1_7 =  ~P1_7; 
                      Delay(3);
                      P1_7=  ~P1_7;
                  }
            }            
    }
    else if(packet_cmd == set_secen_Cmd )
    {
        osal_nv_item_init(Double_Control_List,22,NULL);
        scene_falg |= data[0];
        osal_nv_write(Double_Control_List,5,1,&scene_falg);
        
         if(scene_falg & 0x01)
        {
            memcpy(key2_scene_flag,data+1,16);
            bind_flag &=  ~(0x01 ) ;
            bind_MS_flag &=  ~(0x01 ) ;
            osal_nv_write(Double_Control_List,0,1,&bind_flag);
            osal_nv_write(Double_Control_List,4,1,&bind_MS_flag);
            osal_nv_write(Double_Control_List,6,16,key2_scene_flag);
        }

        end_status_to_coor(0x01,data+1,16,ack_set_secen_Cmd);
        
    }
    else if(packet_cmd == delete_secen_Cmd )
    {
        osal_nv_item_init(Double_Control_List,22,NULL);
        scene_falg &= ~(data[0]);
        osal_nv_write(Double_Control_List,5,1,&scene_falg);
        end_status_to_coor(data[0],data+1,16,ack_delete_secen_Cmd);       
    }
    
}
