#ifndef __SWITCH_H__
#define __SWITCH_H__

#define SWITCH_ENDPOINT           20//1~240
#define SWITCH_PROFID             0x0F08
#define SWITCH_DEVICEID           0x0001
#define SWITCH_DEVICE_VERSION     0
#define SWITCH_FLAGS              0
#define SWITCH_MAX_INCLUSTERS     1
#define SmartHome_CLUSTERID       1
#define SmartHome_Stuts_SEND            0x0002
#define SmartHome_HeartTicket_SEND      0x0004
#define STOP_JOIN_NWK                   0x0010

#define LED2_Blinking_2s                   0x0100
#define LED2_Blinking_4s                   0x0200
#define LED2_Blinking_6s                   0x0400
#define Stop_Bind                          0x1000
#define GATEWAY_GROUP         0x0253 //���
#include "ZDProfile.h"
typedef struct 
{
    uint8 switch_stuts_1;
    uint8 switch_stuts_2;
    uint8 switch_stuts_3;
    uint8 switch_stuts_flage;

}Switch_Stuts_Buff;

void My_Light_Init( byte task_id );
void rxCB(uint8 port,uint8 event);
UINT16 My_Light_event_loop( byte task_id, UINT16 events );
void SWITCH_deal_CoorData(afIncomingMSGPacket_t *pkt);
void Delay(unsigned char n);

extern Switch_Stuts_Buff end_switch_stuts_buff;

extern void control_switch(uint8 switch_num , bool ret);
extern void control_bind_switch(uint8 switch_num , bool ret);
extern void switch_stuts_send(void);
extern void Send_bind_data(uint8 *theMessageData , uint8 theMessageData_len) ;
extern void Send_nwk_date(uint8 *theMessageData , uint8 theMessageData_len);
extern void switch_stuts_bind_send( uint8 key_num);
extern byte SmartHome_TaskID;
extern byte SmartHome_TransID;
extern endPointDesc_t end_epDesc;
extern afAddrType_t DstAddr;  

extern uint8 key2_bind_flag ;
extern uint8 key2_scene_flag[] ;

    
#endif