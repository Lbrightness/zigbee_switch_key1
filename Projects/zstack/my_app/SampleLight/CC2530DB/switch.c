#include "ioCC2530.h"
#include "OSAL.h"
#include "ZDApp.h"
#include "ZDObject.h"
#include "MT_SYS.h"
#include "MT_UART.h"
#include "MT.h"
#include "nwk_util.h"
#include "zcl.h"
#include "zcl_general.h"
#include "zcl_ha.h"
#include "zcl_diagnostic.h"
#include "z_pro_app.h"
#include "z_protocol.h"

#include "AF.h"
#include "hal_lcd.h"
#include "hal_led.h"
#include "hal_key.h"
#include "hal_uart.h"
#include "hal_drivers.h"
#include "NLMEDE.h"
#include "string.h"
#include "OnBoard.h"
#include "onboard.h"
#include "switch.h"
#include "string.h"
#include "stdlib.h"
#include "OSAL_Nv.h"



int af_type_flag = 0;
byte SmartHome_TaskID;
byte SmartHome_TransID;
endPointDesc_t end_epDesc;
afAddrType_t DstAddr;  
devStates_t SmartHome_NwkState;
Switch_Stuts_Buff end_switch_stuts_buff;

uint8 key2_bind_flag = 0;

uint8 key2_scene_flag[16] = {0};


unsigned char uartbuf[30];
void Deal_KeyChange_func(uint8 shift, uint8 keys);
void relay_init(void);

const cId_t SWITCH_InClusterList[1]=
{
  SmartHome_CLUSTERID
};

const SimpleDescriptionFormat_t switch_SimpleDesc =
{
  SWITCH_ENDPOINT,                  
  SWITCH_PROFID,                    
  SWITCH_DEVICEID,
  SWITCH_DEVICE_VERSION,            
  SWITCH_FLAGS,                    
  SWITCH_MAX_INCLUSTERS,         
  (cId_t *)SWITCH_InClusterList, 
  SWITCH_MAX_INCLUSTERS,       
  (cId_t *)SWITCH_InClusterList 
};

void My_Light_Init( byte task_id )
{
  SmartHome_NwkState = DEV_INIT;
  SmartHome_TaskID = task_id;
  SmartHome_TransID = 0;
 
  halUARTCfg_t uartConfig; 
  uartConfig.configured  = TRUE;
  uartConfig.baudRate    = HAL_UART_BR_115200;
  uartConfig.flowControl = FALSE;
  uartConfig.callBackFunc= rxCB;
  HalUARTOpen(0,&uartConfig);
  
  end_epDesc.endPoint = SWITCH_ENDPOINT;
  end_epDesc.task_id = &SmartHome_TaskID;
  end_epDesc.simpleDesc = (SimpleDescriptionFormat_t *)&switch_SimpleDesc;
  end_epDesc.latencyReq = noLatencyReqs;
  afRegister(&end_epDesc);
 
  z_peotocol_init(&z_protocol_info,switch_product_id);                 //赋值mac，shortaddr，
  RegisterForKeys(SmartHome_TaskID);//注册按键
  relay_init(); //初始化继电器
  osal_start_timerEx(SmartHome_TaskID,STOP_JOIN_NWK,60000);
}
 
UINT16 My_Light_event_loop( byte task_id, UINT16 events )
{
  
  afIncomingMSGPacket_t  *MSGpkt;
  (void)task_id;
//  uint8 sentStatus;
   
  if( events & SYS_EVENT_MSG )
  {
    while( (MSGpkt = (afIncomingMSGPacket_t *)osal_msg_receive(SmartHome_TaskID) ) )
    {
      switch( MSGpkt->hdr.event )
      {
        case AF_INCOMING_MSG_CMD:      
        SWITCH_deal_CoorData(MSGpkt);
         
         //HalUARTWrite(0,MSGpkt->cmd.Data,MSGpkt->cmd.DataLength);///////////////////////////////////////
          break;
           
          case ZDO_STATE_CHANGE:
          SmartHome_NwkState = (devStates_t)(MSGpkt->hdr.status);
          if(SmartHome_NwkState == DEV_ROUTER ) //入网成功
          {
               //HalUARTWrite(0,"enddevice into nwk\n",19);///////////////////////////////////      
               //NLME_PermitJoiningRequest(0); //禁止入网
               osal_stop_timerEx(SmartHome_TaskID,STOP_JOIN_NWK);
               P1_7 = 0;
               
               end_Attributes_to_cood_Func(&z_protocol_info ,connect_success_up_Attributes_Cmd);
               osal_start_timerEx(SmartHome_TaskID,SmartHome_HeartTicket_SEND,3000);
                
               uint8 bind_nv_falg = 0;
               osal_nv_item_init(Double_Control_List,22,NULL);
               osal_nv_read(Double_Control_List,0,1,&bind_nv_falg);
               if((bind_nv_falg != 0)&&(bind_nv_falg != 0xFF))
               {
                    bind_flag = bind_nv_falg;
                       
                    osal_nv_read(Double_Control_List,1,1,&key2_bind_flag);          
                    uint8 bind_shortaddr[2]={0};
                    osal_nv_read(Double_Control_List,2,2,bind_shortaddr);
                    DstAddr.addr.shortAddr = (bind_shortaddr[0]*256) + (bind_shortaddr[1]);
                    
                    osal_nv_read(Double_Control_List,4,1,&bind_MS_flag);
               }
               else
               {
                   bind_flag = 0x00;
                   bind_MS_flag = 0x00;
               }
              
               uint8 scene_nv_falg = 0;
               osal_nv_read(Double_Control_List,5,1,&scene_nv_falg);
               if((scene_nv_falg != 0)&&(scene_nv_falg != 0xFF))
               {
                   scene_falg = scene_nv_falg;
                   osal_nv_read(Double_Control_List,6,16,key2_scene_flag);   
               }
               else
               {
                   scene_falg = 0;
               }
               
          }
          else if(SmartHome_NwkState == DEV_NWK_ORPHAN )//没有协调器消息
          {
               P1_7 = 1;  
          }
          break;
          
          case AF_DATA_CONFIRM_CMD:   
//              sentStatus = MSGpkt->hdr.status;
//              if( af_type_flag == 0)      
//              {
//                      if ( sentStatus != ZSuccess )
//                      {
//                                      P1_7 = 1; 
//                      }
//                      else 
//                      {
//                                      P1_7 = 0;
//
//                      }
//              }
              
          break;
          
         case KEY_CHANGE:
           Deal_KeyChange_func(((keyChange_t *)MSGpkt)->state, ((keyChange_t *)MSGpkt)->keys);
           break;
        
          default:
            break;
      }
      osal_msg_deallocate((uint8*)MSGpkt);
    }
    return(events ^ SYS_EVENT_MSG);
  }
  if(events & SmartHome_Stuts_SEND)
  {
    switch_stuts_send();
    return(events ^ SmartHome_Stuts_SEND);
  }

  if(events & SmartHome_HeartTicket_SEND)
  {   
    /*osal_stop_timerEx( SmartHome_TaskID,STOP_JOIN_NWK );*/
    
    end_Attributes_to_cood_Func(&z_protocol_info , end_hearstick_cmd);//发送属性给协调器
    osal_start_timerEx(SmartHome_TaskID,SmartHome_HeartTicket_SEND,20000);//30s发送一次心跳包
    return(events ^ SmartHome_HeartTicket_SEND);
  }
  if(events & STOP_JOIN_NWK)
  {
      ZDApp_StopJoiningCycle();
      return(events ^ STOP_JOIN_NWK);
  }
  if(events & Stop_Bind)
  {
      bind_flag &=  ~(0xF0 ) ;
      return(events ^ Stop_Bind);
  }

  if(events & LED2_Blinking_2s)
  {
      uint8 led_times2_2s = 2;
      while(led_times2_2s--)
      {
          Delay(5);
          P1_6 =  ~P1_6; 
          Delay(5);
          P1_6 =  ~P1_6;
      }
      return(events ^ LED2_Blinking_2s);
  }
  if(events & LED2_Blinking_4s)
  {
      uint8 led_times2_4s = 4;
      while(led_times2_4s--)
      {
          Delay(5);
          P1_6 =  ~P1_6; 
          Delay(5);
          P1_6 =  ~P1_6;
      }
      return(events ^ LED2_Blinking_4s);
  }
  if(events & LED2_Blinking_6s)
  {
      uint8 led_times2_6s = 6;
      while(led_times2_6s--)
      {
          Delay(5);
          P1_6 =  ~P1_6; 
          Delay(5);
          P1_6 =  ~P1_6;
      }
      return(events ^ LED2_Blinking_6s);
  }
  
   return 0;
}

   
void SWITCH_deal_CoorData(afIncomingMSGPacket_t *pkt)
{
      switch(pkt->clusterId)
      {
        case SmartHome_CLUSTERID:
          end_analysis_coor_Func(pkt->cmd.Data,pkt->LinkQuality);  
        break;  
      }
}


 void Send_bind_data(uint8 *theMessageData , uint8 theMessageData_len)  //应答绑定请求以及发送消息
{   
    //af_type_flag = 1;
    DstAddr.addrMode       = (afAddrMode_t)Addr16Bit;  
    DstAddr.endPoint       = SWITCH_ENDPOINT;  
    //DstAddr.addr.shortAddr = 0x49BB;      
    AF_DataRequest(&DstAddr,&end_epDesc,SmartHome_CLUSTERID,theMessageData_len,theMessageData,&SmartHome_TransID,AF_DISCV_ROUTE,AF_DEFAULT_RADIUS);
    
} 

void Send_nwk_date(uint8 *theMessageData , uint8 theMessageData_len)
{
    //af_type_flag = 0;
    afAddrType_t DstAddr_nwk;
    DstAddr_nwk.addrMode = (afAddrMode_t)AddrGroup;
    DstAddr_nwk.endPoint = 20;//endPoint
    DstAddr_nwk.addr.shortAddr = GATEWAY_GROUP;
    
    AF_DataRequest(&DstAddr_nwk,&end_epDesc,SmartHome_CLUSTERID,theMessageData_len,theMessageData,&SmartHome_TransID,AF_DISCV_ROUTE|AF_ACK_REQUEST ,AF_DEFAULT_RADIUS);
    
}

void Deal_KeyChange_func(uint8 shift, uint8 keys)
{
   
  if ( keys & HAL_KEY_SW_6 )
  {
      if(shift == 0x01)
      {
          if(scene_falg & 0x01)
          {
              P1_6 = 1;
              end_status_to_coor(0x01,key2_scene_flag,16,contorl_secen_Cmd); 
              P1_2 = 0;
              Delay(8);
              P1_2 = 1;
          }
          else
          {
              P1_6 =  ~P1_6; 
              //if(!(bind_MS_flag & 0x01))
             // {
                  P1_3 = P1_6;
             // }
              switch_stuts_bind_send(0x01);
          }       
      }
      else if(shift == 0x02)
      {     
          bind_flag &=  ~(0xF0 ) ;
          bind_flag |= 0x10;
          end_bind_Func(0x01,end_bind_Cmd); 
          osal_start_timerEx(SmartHome_TaskID,Stop_Bind,13000);
      }
      else if(shift == 0x03)
      {     
          //解绑key2  
          bind_flag &=  ~(0x01 ) ;
          bind_MS_flag &=  ~(0x01 ) ;
          osal_nv_item_init(Double_Control_List,22,NULL);
          osal_nv_write(Double_Control_List,0,1,&bind_flag);
          osal_nv_write(Double_Control_List,4,1,&bind_MS_flag);
      }
      else if(shift == 0x04)
      {     
           uint8 clean_flsh_date[22]={0};
          osal_nv_item_init(Double_Control_List,22,NULL);
          osal_nv_write(Double_Control_List,0,22,clean_flsh_date);
          zgWriteStartupOptions(ZG_STARTUP_SET, ZCD_STARTOPT_DEFAULT_NETWORK_STATE);
          SystemReset();  
      }
  }

}

void relay_init(void)
{

  
  P1SEL &= ~0x40; //通用（0）还是外设（1）
  P1DIR |= 0x40;//输入（0）还是输出（1）
  P1_6 = 0;
  
  P1SEL &= ~0x80; //通用（0）还是外设（1）
  P1DIR |= 0x80;//输入（0）还是输出（1）
  P1_7 = 1;
  
  P1SEL &= ~0x08; //通用（0）还是外设（1）---relay1
  P1DIR |= 0x08;//输入（0）还是输出（1）
  P1_3 = 0;
  
   P1SEL &= ~0x04; //通用（0）还是外设（1）
   P1DIR |= 0x04;//输入（0）还是输出（1）
   P1_2 = 1;//LED2
}

void control_switch(uint8 switch_num , bool ret)
{
 
  if( switch_num == 0x01)
  {
      P1_6 = ret; 
     // if(!(bind_MS_flag & 0x01))
     // {
          P1_3 = ret;
     // }
      switch_stuts_send();
  }
}

void control_bind_switch(uint8 switch_num , bool ret)
{
 
  if( switch_num == 0x01)
  {
      if(scene_falg & 0x01)
      {
          P1_6 = 1;
         // end_status_to_coor(0x01,key2_scene_flag,16,contorl_secen_Cmd); 
          switch_stuts_send();
      }
      else
      {
            P1_6 = ret;  
            P1_3 = ret;
            switch_stuts_bind_send(0x01);
      }
  }
  
}

void switch_stuts_send(void)
{
    uint8 switch_stuts_num = P1_6;
  
    end_status_to_coor(0x01,&switch_stuts_num,1,Dev_status_Cmd);
}

void switch_stuts_bind_send( uint8 key_num)
{
        uint8 switch_stuts_num;
        if((key_num == 0x01)&&(bind_flag & 0x01))
        {
            
            switch_stuts_num = P1_6;
            end_status_to_coor(key2_bind_flag,&switch_stuts_num,1,0xC2);
        }       
        switch_stuts_send();  
}

static void rxCB(uint8 port,uint8 event)
{
    uint16 length;
    osal_memcpy(uartbuf,0,30);
    length = Hal_UART_RxBufLen(0);
    if(length)
    {
       HalUARTRead(0,uartbuf,length);
//       if(osal_memcmp("bind",uartbuf,length))
//       {
//           bind_flag = 1;
//           end_bind_Func(0x01,end_bind_Cmd);
//       }
//       else
//       {
//           Send_bind_data(uartbuf,length);
//       }
       
    }
    
}

void Delay(unsigned char n)  
{
  unsigned char i;
  unsigned int j;
  for(i = 0; i < n; i++)
    for(j = 1; j; j++)
    ;
}  